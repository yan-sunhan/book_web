package com.book_web;

import com.book_web.util.SpringUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class InmWebApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(InmWebApplication.class, args);
        SpringUtil.setApplicationContext(applicationContext);
        SpringUtil.printBean();
    }

}
