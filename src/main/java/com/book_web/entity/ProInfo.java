package com.book_web.entity;

public class ProInfo {
    public int pro_id;
    public String pro_name;
    public float pro_price;
    public String pro_num;


    public int getPro_id() { return pro_id;}

    public void setPro_id(int pro_id){ this.pro_id = pro_id;}

    public String getPro_name() { return pro_name;}

    public void setPro_name(String pro_name){ this.pro_name = pro_name;}

    public float getPro_price() { return pro_price;}

    public void setPro_price(float pro_price){ this.pro_price = pro_price;}

    public String getPro_num() {
        return pro_num;
    }

    public void setPro_num(String pro_num) {
        this.pro_num = pro_num;
    }
}
