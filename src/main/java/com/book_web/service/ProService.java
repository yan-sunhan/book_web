package com.book_web.service;

import com.book_web.entity.ProInfo;
import com.book_web.mapper.ProMapper;
import com.book_web.util.SpringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProService {
    @Autowired
    private ProMapper proMapper;
    public List<ProInfo> getAllProList(){
        String sqlTxt = "select * from pro_info";
        List<ProInfo> list = new ArrayList<ProInfo>();
        JdbcTemplate jdbcTemplate = (JdbcTemplate)
                SpringUtil.applicationContext.getBean("jdbcTemplate");
        List<Map<String, Object>> stu_list = jdbcTemplate.queryForList(sqlTxt);
        for (Map<String, Object> map : stu_list) {
            ProInfo proInfo = new ProInfo();
            proInfo.setPro_id(Integer.parseInt(map.get("pro_id").toString()));
            proInfo.setPro_name(map.get("pro_name").toString());
            proInfo.setPro_price(Float.parseFloat(map.get("pro_price").toString()));
            proInfo.setPro_num(map.get("pro_num").toString());
            list.add(proInfo);
        }
        return list;
    }
    public int deleteAPro(int pro_id){ return proMapper.deleteAPro(pro_id); }

    public int insertAPro(ProInfo proInfo){
        return proMapper.insertAPro(proInfo);
    }

    public int updateAPro(ProInfo proInfo){ return proMapper.updateAPro(proInfo); }

    public ProInfo queryProById(int pro_id){ return proMapper.queryProById(pro_id); }

    public ProInfo queryProByName(String pro_name){ return proMapper.queryProByName(pro_name); }
}
