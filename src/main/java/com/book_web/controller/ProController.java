package com.book_web.controller;

import com.book_web.entity.ProInfo;
import com.book_web.service.ProService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ProController {
    @Autowired
    private ProService proService;

    @RequestMapping("show_pro_list")
    public String showProList(HttpServletRequest request, Model model){
        List<ProInfo> proInfoList = proService.getAllProList();
        model.addAttribute("pro_list",proInfoList);
        return "pro_list";
    }
    @RequestMapping("delete_a_pro")
    public String deleteAPro(HttpServletRequest request,Model model,int pro_id){

        int count = proService.deleteAPro(pro_id);
        if (count > 0) {
            return "redirect:show_pro_list";
        } else {
            return "redirect:show_pro_list";
        }
    }
    @RequestMapping("insert_a_pro")
    public String insertAPro(ProInfo proInfo, HttpServletRequest request, RedirectAttributes redirectAttributes){

        proService.insertAPro(proInfo);
        return "redirect:show_pro_list";
    }
    @RequestMapping("to_insert")
    public String toInsert(){
        return "add_pro";
    }


    @RequestMapping("to_update")
    public String toUpdate(int pro_id,Model model){
        ProInfo proInfo = proService.queryProById(pro_id);
        model.addAttribute("proInfo",proInfo);
        return "update_pro";
    }

    @RequestMapping("update_a_pro")
    public String updateAPro(ProInfo proInfo){
        proService.updateAPro(proInfo);
        return "redirect:show_pro_list";
    }

    @RequestMapping("quary_a_pro")
    public String quaryAPro(String pro_name,Model model){
        ProInfo proInfo = proService.queryProByName(pro_name);
        List<ProInfo> proInfoList = new ArrayList<ProInfo>();
        proInfoList.add(proInfo);
        model.addAttribute("pro_list",proInfoList);
        return "pro_list";
    }
}
