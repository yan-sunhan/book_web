package com.book_web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AdminController {

    @RequestMapping("show_login")
    public String showLogin(){
        return "login";
    }

    @RequestMapping("check_login")
    public String checkLogin(HttpServletRequest request){
        String userName = request.getParameter("user_name");
        String password = request.getParameter("pwd");
        if (userName.equals("123")&& password.equals("123")) {
            return "redirect:show_main";
        } else {
            return "redirect:show_login";
        }
    }
    @RequestMapping("show_main")
    public String showMain(Model model){
        model.addAttribute("admin_name","颜孙瀚");
        model.addAttribute("admin_id","0");
        return "main";
    }

}
