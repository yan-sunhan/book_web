package com.book_web.mapper;

import com.book_web.entity.ProInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProMapper {
    @Select("select * from pro_info")
    List<ProInfo> getProList();

    @Delete("delete from pro_info where pro_id = #{pro_id}")
    int deleteAPro(@Param("pro_id") int pro_id);

    @Insert("insert into pro_info(pro_id, pro_name, pro_price, pro_num)\n" +
            "        values (#{pro_id}, #{pro_name}, #{pro_price}, #{pro_num})")
    int insertAPro(ProInfo proInfo);


    int updateAPro(ProInfo proInfo);

    ProInfo queryProById(int pro_id);

    ProInfo queryProByName(String pro_name);
}
